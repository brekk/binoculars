import {
  identity as I,
  curry,
  symmetricDifference,
  apply,
  concat,
  filter,
  fromPairs,
  keys,
  map,
  pipe,
  prop,
  toPairs,
  uniq
} from "ramda"
// import { trace } from "xtrace"

import { localName, relativePathLocalName } from "./path"
import { contains, hap, sort } from "./utils"
import { isThirdParty, isBuiltin } from "./filter"

// ----- private
export const changePairs = curry((fn, x) =>
  pipe(
    toPairs,
    map(fn),
    fromPairs
  )(x)
)

export const thirdPartyName = x => {
  const latter = x.slice(x.indexOf("node_modules/"))
  const module = latter.slice(0, latter.indexOf("/", latter.indexOf("/") + 1))
  return module.slice(module.indexOf("/") + 1)
}

export const cleanName = k =>
  isThirdParty(k) ? thirdPartyName(k) : isBuiltin(k) ? k : localName(k)

export const getSimplifiedImports = pipe(
  prop("imports"),
  changePairs(([k, v]) => [cleanName(k), v])
)

export const getBuiltinModules = pipe(
  prop("imports"),
  keys,
  filter(isBuiltin)
)
export const getModules = pipe(
  prop("imports"),
  keys,
  filter(contains("node_modules")),
  map(thirdPartyName)
)
export const getAllModules = pipe(
  hap([getBuiltinModules, getModules]),
  apply(concat),
  uniq,
  sort
)
export const getExports = pipe(
  prop("exports"),
  changePairs(([k, v]) => [relativePathLocalName(k), sort(v)])
)
// loop through exports and compare to find locally-used only
export const usedLocallyOnly = curry((exports, imports) =>
  pipe(
    toPairs,
    map(([k, v]) => {
      const imp = imports[k]
      if (!imp) return false
      const diff = symmetricDifference(v, imp)
      if (diff.length === 0) return false
      return [k, diff]
    }),
    filter(I),
    fromPairs
  )(exports)
)

// ----- public
export const transformer = raw => {
  const imports = getSimplifiedImports(raw)
  const exports = getExports(raw)
  const modules = getAllModules(raw)
  const usedLocally = usedLocallyOnly(exports, imports)
  return { imports, exports, private: usedLocally, modules }
}
