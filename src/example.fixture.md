# example.js

## lint
* Use .forEach instead of for-loops !lint:loops
* Use map instead of .map !lint:map

// this is a comment
## dependencies
* `import {map} from "ramda"` !add

