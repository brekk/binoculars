// import { e2 } from "entrust"
import { curry, pipe, ap } from "ramda"

// ----- private
// export const fork = e2("fork")
export const box = x => [x]

// ----- public
export const fork = curry((bad, good, ff) => ff.fork(bad, good))
export const contains = curry((y, x) => ~x.indexOf(y))

export const hap = curry((fns, x) =>
  pipe(
    box,
    ap(fns)
  )(x)
)
export const sort = x => x.sort()
