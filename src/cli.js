import argify from "yargs-parser"
import { flip } from "ramda"

export const readArgv = flip(argify)
