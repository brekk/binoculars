import fs from "fs"

import {
  pathEq,
  pathOr,
  ap,
  filter,
  identity as I,
  propOr,
  prop,
  reduce,
  flip,
  map,
  pipe,
  pluck,
  curry
} from "ramda"
import F from "fluture"
import getImportsExports from "get-es-imports-exports"
import builtins from "builtin-modules"
import extractComments from "extract-comments"
import doctrine from "doctrine"

import { trace } from "xtrace"

import { relativePath } from "./path"
import { box } from "./utils"

// ----- private
export const getIE = F.encaseP(getImportsExports)
export const readFile = F.encaseN2(fs.readFile)
export const readUTF8 = x => readFile(x, "utf8")

export const binaryAssign = (a, b) => Object.assign({}, a, b)
export const merge = curry(binaryAssign)
export const flipMerge = flip(binaryAssign)

/**
 * Parse raw comments using doctrine
 * @method parseComment
 * @param {String} x - comment
 * @returns {Object} parsed comments
 */
export const parseComment = doctrine.parse
// doctrine.parse(x, {
// unwrap: true,
// recoverable: true,
// sloppy: true
// })

// ----- public
export const getImportsAndExports = files =>
  getIE({ files, parser: "babel-eslint", exclude: builtins })

export const transformTags = reduce((acc, x) => {
  console.log(acc, "<>>", x)
  return acc.concat(x)
}, [])
const isExpWrapped = tag =>
  pathEq(["type", "type"], "TypeApplication", tag) &&
  pipe(
    pathOr(false, ["type", "expression", "name"]),
    Boolean
  )(tag)

const getRawType = pathOr("Unknown", ["type", "name"])

const wrapWithArray = pipe(
  box,
  ap([
    pathOr("UnknownList", ["type", "expression", "name"]),
    // pathOr("Unknown", ["type", "applications", 0, "name"])
    pipe(
      pathOr([], ["type", "applications"]),
      pluck("name")
    )
  ]),
  ([Acc, X]) => `${Acc}<${X}>`
)
const getType = z => (isExpWrapped(z) ? wrapWithArray(z) : getRawType(z))

const getTagData = pipe(
  box,
  ap([propOr("Unknown", "title"), getType, prop("name"), prop("description")])
)

const getNormalizedTags = x =>
  pipe(
    getTagData,
    ([tag, type, name, description]) => ({ tag, type, name, description })
  )(x)

const cleanNormalizedTags = x => pipe(merge({}))(x)

/**
 * Convert a string path to an array of comment objects
 * @method getComments
 * @param {String} file - string path
 * @return {Object[]} array of comment objects
 */
export const getComments = file =>
  pipe(
    relativePath,
    // :: file path
    readUTF8,
    // :: Future<utf8>
    map(
      pipe(
        // :: <raw>
        extractComments,
        // :: <[comment]>
        pluck("value"),
        // :: <[comment.value]>
        map(
          // :: <comment>
          pipe(
            parseComment,
            // :: <comment-object>
            merge({ file }),
            z =>
              merge(z, {
                tags: pipe(
                  propOr([], "tags"),
                  map(
                    pipe(
                      getNormalizedTags,
                      cleanNormalizedTags
                    )
                  )
                )(z)
              })
            // :: <comment + file>
          )
        )
      )
    )
  )(file)
