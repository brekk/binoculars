import builtins from "builtin-modules"
import { pipe, map, reject } from "ramda"

import { relativePath } from "./path"
import { contains } from "./utils"

// ----- private
export const specFiles = x => contains("spec", x) || contains("test", x)

// ----- public
export const isBuiltin = x => ~builtins.indexOf(x)
export const isThirdParty = contains("node_modules")
export const sourceFilesOnly = pipe(
  map(relativePath),
  reject(specFiles)
)
