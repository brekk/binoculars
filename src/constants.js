// ----- private
export const yargsOpts = {
  alias: {
    color: ["k"],
    verbose: ["v"]
  },
  default: {
    color: true
  },
  count: ["v"],
  number: "v".split("")
}

// ----- public
export const CONFIG = {
  name: `binoculars`,
  yargsOpts,
  descriptions: {}
}
