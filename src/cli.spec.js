import path from "path"
import { testCommand, is } from "sinew/testing"
import { pipe } from "ramda"

const exe = path.resolve(`../binoculars.js`)

const fixRelative = x => {
  const lastSlash = x.relative.lastIndexOf(`/`)
  const lastTwoSlashes = x.relative.lastIndexOf(`/`, lastSlash)
  const lastThreeSlashes = x.relative.lastIndexOf(`/`, lastTwoSlashes)
  x.relative = x.relative.substr(lastThreeSlashes)
  return x
}

const fixture = `example.fixture.md`

const RUN = {
  read: [exe, `--read`, fixture],
  tags: [exe, `--tags`, fixture],
  tagKind: [exe, `--tags`, fixture, `--tagKind`, `lint`]
}

testCommand(
  RUN.read,
  pipe(
    fixRelative,
    is({
      title: `example.js`,
      relative: `/path/to/example.js`,
      content: {
        lint: [
          {
            tags: [{ kind: `lint`, tag: `loops` }],
            value: `Use .forEach instead of for-loops`
          },
          {
            tags: [{ kind: `lint`, tag: `maps` }],
            value: `Use map instead of .map`
          }
        ],
        dependencies: [
          {
            tags: [`add`],
            value: `import {map} from "ramda"`
          }
        ]
      }
    })
  )
)

testCommand(
  RUN.tags,
  is([
    { kind: `lint`, tag: `loops` },
    { kind: `lint`, tag: `maps` },
    { tag: `add` }
  ])
)

testCommand(RUN.tagKind, is([`loops`, `maps`]))
