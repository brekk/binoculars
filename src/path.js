import path from "path"

import F from "fluture"
import glob from "fast-glob"
import { pipe } from "ramda"

import { contains } from "./utils"

// ----- private
export const readGlob = F.encaseP(glob)
export const runningDir = process
  .cwd()
  .slice(process.cwd().lastIndexOf(path.sep) + 1)

// ----- public
export const relativePath = x => path.relative(process.cwd(), x)
export const localName = x => path.join(runningDir, relativePath(x))
export const relativePathLocalName = pipe(
  relativePath,
  localName
)

export const forceGlob = x => {
  if (!contains("*", x)) x += "/*.js"
  return readGlob(x)
}
