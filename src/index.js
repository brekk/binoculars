import { reduce, concat, chain, map, pathOr, propOr, pipe, filter } from "ramda"
// import { e2 } from "entrust"
// import { trace } from "xtrace"
import F from "fluture"

import { readArgv } from "./cli"
import { CONFIG } from "./constants"
import { sourceFilesOnly } from "./filter"
import { getImportsAndExports, getComments } from "./parser"
import { forceGlob } from "./path"
import { transformer } from "./transform"
import { fork } from "./utils"

const log = console.log

const getProjectDetails = pipe(
  chain(getImportsAndExports),
  map(transformer)
)

const getJSDocComments = pipe(
  map(chain(getComments)),
  chain(F.parallel(10)),
  map(
    pipe(
      map(filter(pathOr(false, ["tags", "length"]))),
      filter(propOr(false, "length"))
    )
  ),
  map(reduce(concat, []))
)

/**
 * This is the core binoculars function
 * @method binoculars
 * @param {string[]} argv - process.argv values
 * @return {Future<project>} - future wrapped project object
 * @example
 * $ binoculars src
 */
const binoculars = pipe(
  readArgv(CONFIG.yargsOpts),
  pathOr([], ["_", 0]),
  forceGlob,
  map(sourceFilesOnly),
  // consider using ap & F.Par,
  z => [getProjectDetails(z), getJSDocComments(z)],
  F.parallel(2),
  map(([project, comments]) => ({ project, comments })),
  map(x => JSON.stringify(x, null, 2)),
  fork(log, log)
)

export default binoculars

binoculars(process.argv.slice(2))
