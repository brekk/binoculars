#!/usr/bin/env node
'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var ramda = require('ramda');
var F = _interopDefault(require('fluture'));
var argify = _interopDefault(require('yargs-parser'));
var path = _interopDefault(require('path'));
var glob = _interopDefault(require('fast-glob'));
var fs = _interopDefault(require('fs'));
var getImportsExports = _interopDefault(require('get-es-imports-exports'));
var extractComments = _interopDefault(require('extract-comments'));
var doctrine = _interopDefault(require('doctrine'));

var readArgv = ramda.flip(argify);

var yargsOpts = {
  alias: {
    color: ["k"],
    verbose: ["v"]
  },
  default: {
    color: true
  },
  count: ["v"],
  number: "v".split("")
};
var CONFIG = {
  name: "binoculars",
  yargsOpts: yargsOpts,
  descriptions: {}
};

var blacklist = [
	'freelist',
	'sys'
];
var builtinModules = Object.keys(process.binding('natives')).filter(function (el) {
	return !/^_|^internal|\//.test(el) && blacklist.indexOf(el) === -1;
}).sort();

var box = function (x) { return [x]; };
var fork = ramda.curry(function (bad, good, ff) { return ff.fork(bad, good); });
var contains = ramda.curry(function (y, x) { return ~x.indexOf(y); });
var hap = ramda.curry(function (fns, x) { return ramda.pipe(
    box,
    ramda.ap(fns)
  )(x); }
);
var sort = function (x) { return x.sort(); };

var readGlob = F.encaseP(glob);
var runningDir = process
  .cwd()
  .slice(process.cwd().lastIndexOf(path.sep) + 1);
var relativePath = function (x) { return path.relative(process.cwd(), x); };
var localName = function (x) { return path.join(runningDir, relativePath(x)); };
var relativePathLocalName = ramda.pipe(
  relativePath,
  localName
);
var forceGlob = function (x) {
  if (!contains("*", x)) { x += "/*.js"; }
  return readGlob(x)
};

var specFiles = function (x) { return contains("spec", x) || contains("test", x); };
var isBuiltin = function (x) { return ~builtinModules.indexOf(x); };
var isThirdParty = contains("node_modules");
var sourceFilesOnly = ramda.pipe(
  ramda.map(relativePath),
  ramda.reject(specFiles)
);

var PLACEHOLDER = "🍛";
var $ = PLACEHOLDER;
var bindInternal3 = function bindInternal3 (func, thisContext) {
  return function (a, b, c) {
    return func.call(thisContext, a, b, c);
  };
};
var some$1 = function fastSome (subject, fn, thisContext) {
  var length = subject.length,
      iterator = thisContext !== undefined ? bindInternal3(fn, thisContext) : fn,
      i;
  for (i = 0; i < length; i++) {
    if (iterator(subject[i], i, subject)) {
      return true;
    }
  }
  return false;
};
var curry = function (fn) {
  var test = function (x) { return x === PLACEHOLDER; };
  return function curried() {
    var arguments$1 = arguments;
    var argLength = arguments.length;
    var args = new Array(argLength);
    for (var i = 0; i < argLength; ++i) {
      args[i] = arguments$1[i];
    }
    var countNonPlaceholders = function (toCount) {
      var count = toCount.length;
      while (!test(toCount[count])) {
        count--;
      }
      return count
    };
    var length = (
      some$1(args, test) ?
        countNonPlaceholders(args) :
        args.length
    );
    function saucy() {
      var arguments$1 = arguments;
      var arg2Length = arguments.length;
      var args2 = new Array(arg2Length);
      for (var j = 0; j < arg2Length; ++j) {
        args2[j] = arguments$1[j];
      }
      return curried.apply(this, args.map(
        function (y) { return (
          test(y) && args2[0] ?
            args2.shift() :
            y
        ); }
      ).concat(args2))
    }
    return (
      length >= fn.length ?
        fn.apply(this, args) :
        saucy
    )
  }
};
var innerpipe = function (args) { return function (x) {
  var first = args[0];
  var rest = args.slice(1);
  var current = first(x);
  for (var a = 0; a < rest.length; a++) {
    current = rest[a](current);
  }
  return current
}; };
function pipe() {
  var arguments$1 = arguments;
  var argLength = arguments.length;
  var args = new Array(argLength);
  for (var i = 0; i < argLength; ++i) {
    args[i] = arguments$1[i];
  }
  return innerpipe(args)
}
var prop = curry(function (property, o) { return o && property && o[property]; });
var _keys = Object.keys;
var keys = _keys;
var propLength = prop("length");
var objectLength = pipe(keys, propLength);
var delegatee = curry(function (method, arg, x) { return (x[method](arg)); });
var filter = delegatee("filter");
function curryObjectN(arity, fn) {
  return function λcurryObjectN(args) {
    var joined = function (z) { return λcurryObjectN(Object.assign({}, args, z)); };
    return (
      args && Object.keys(args).length >= arity ?
        fn(args) :
        joined
    )
  }
}

var callWithScopeWhen = curry(function (effect, when, what, value) {
  var scope = what(value);
  if (when(scope)) { effect(scope); }
  return value;
});
var callBinaryWithScopeWhen = curry(function (effect, when, what, tag, value) {
  var scope = what(value);
  if (when(tag, scope)) { effect(tag, scope); }
  return value;
});
var always = function always() {
  return true;
};
var I = function I(x) {
  return x;
};
var callWhen = callWithScopeWhen($, $, I);
var call = callWithScopeWhen($, always, I);
var callWithScope = callWithScopeWhen($, always);
var callBinaryWhen = callBinaryWithScopeWhen($, $, I);
var callBinaryWithScope = callBinaryWithScopeWhen($, always);
var callBinary = callBinaryWithScopeWhen($, always, I);
var traceWithScopeWhen = callBinaryWithScopeWhen(console.log);
var traceWithScope = traceWithScopeWhen(always);
var inspect = traceWithScope;
var trace = inspect(I);
var traceWhen = callBinaryWithScopeWhen(console.log, $, I);
var segment = curryObjectN(3, function (_ref) {
  var _ref$what = _ref.what,
      what = _ref$what === void 0 ? I : _ref$what,
      _ref$when = _ref.when,
      when = _ref$when === void 0 ? always : _ref$when,
      tag = _ref.tag,
      value = _ref.value,
      effect = _ref.effect;
  if (when(tag, what(value))) {
    effect(tag, what(value));
  }
  return value;
});
var segmentTrace = segment({
  effect: console.log
});

var getIE = F.encaseP(getImportsExports);
var readFile = F.encaseN2(fs.readFile);
var readUTF8 = function (x) { return readFile(x, "utf8"); };
var binaryAssign = function (a, b) { return Object.assign({}, a, b); };
var merge = ramda.curry(binaryAssign);
var flipMerge = ramda.flip(binaryAssign);
var parseComment = doctrine.parse;
var getImportsAndExports = function (files) { return getIE({ files: files, parser: "babel-eslint", exclude: builtinModules }); };
var transformTags = ramda.reduce(function (acc, x) {
  console.log(acc, "<>>", x);
  return acc.concat(x)
}, []);
var isExpWrapped = function (tag) { return ramda.pathEq(["type", "type"], "TypeApplication", tag) &&
  ramda.pipe(
    ramda.pathOr(false, ["type", "expression", "name"]),
    Boolean
  )(tag); };
var getRawType = ramda.pathOr("Unknown", ["type", "name"]);
var wrapWithArray = ramda.pipe(
  box,
  ramda.ap([
    ramda.pathOr("UnknownList", ["type", "expression", "name"]),
    ramda.pipe(
      ramda.pathOr([], ["type", "applications"]),
      ramda.pluck("name")
    )
  ]),
  function (ref) {
    var Acc = ref[0];
    var X = ref[1];
    return (Acc + "<" + X + ">");
}
);
var getType = function (z) { return (isExpWrapped(z) ? wrapWithArray(z) : getRawType(z)); };
var getTagData = ramda.pipe(
  box,
  ramda.ap([ramda.propOr("Unknown", "title"), getType, ramda.prop("name"), ramda.prop("description")])
);
var getNormalizedTags = function (x) { return ramda.pipe(
    getTagData,
    function (ref) {
      var tag = ref[0];
      var type = ref[1];
      var name = ref[2];
      var description = ref[3];
      return ({ tag: tag, type: type, name: name, description: description });
    }
  )(x); };
var cleanNormalizedTags = function (x) { return ramda.pipe(merge({}))(x); };
var getComments = function (file) { return ramda.pipe(
    relativePath,
    readUTF8,
    ramda.map(
      ramda.pipe(
        extractComments,
        ramda.pluck("value"),
        ramda.map(
          ramda.pipe(
            parseComment,
            merge({ file: file }),
            function (z) { return merge(z, {
                tags: ramda.pipe(
                  ramda.propOr([], "tags"),
                  ramda.map(
                    ramda.pipe(
                      getNormalizedTags,
                      cleanNormalizedTags
                    )
                  )
                )(z)
              }); }
          )
        )
      )
    )
  )(file); };

var changePairs = ramda.curry(function (fn, x) { return ramda.pipe(
    ramda.toPairs,
    ramda.map(fn),
    ramda.fromPairs
  )(x); }
);
var thirdPartyName = function (x) {
  var latter = x.slice(x.indexOf("node_modules/"));
  var module = latter.slice(0, latter.indexOf("/", latter.indexOf("/") + 1));
  return module.slice(module.indexOf("/") + 1)
};
var cleanName = function (k) { return isThirdParty(k) ? thirdPartyName(k) : isBuiltin(k) ? k : localName(k); };
var getSimplifiedImports = ramda.pipe(
  ramda.prop("imports"),
  changePairs(function (ref) {
    var k = ref[0];
    var v = ref[1];
    return [cleanName(k), v];
})
);
var getBuiltinModules = ramda.pipe(
  ramda.prop("imports"),
  ramda.keys,
  ramda.filter(isBuiltin)
);
var getModules = ramda.pipe(
  ramda.prop("imports"),
  ramda.keys,
  ramda.filter(contains("node_modules")),
  ramda.map(thirdPartyName)
);
var getAllModules = ramda.pipe(
  hap([getBuiltinModules, getModules]),
  ramda.apply(ramda.concat),
  ramda.uniq,
  sort
);
var getExports = ramda.pipe(
  ramda.prop("exports"),
  changePairs(function (ref) {
    var k = ref[0];
    var v = ref[1];
    return [relativePathLocalName(k), sort(v)];
})
);
var usedLocallyOnly = ramda.curry(function (exports, imports) { return ramda.pipe(
    ramda.toPairs,
    ramda.map(function (ref) {
      var k = ref[0];
      var v = ref[1];
      var imp = imports[k];
      if (!imp) { return false }
      var diff = ramda.symmetricDifference(v, imp);
      if (diff.length === 0) { return false }
      return [k, diff]
    }),
    ramda.filter(ramda.identity),
    ramda.fromPairs
  )(exports); }
);
var transformer = function (raw) {
  var imports = getSimplifiedImports(raw);
  var exports = getExports(raw);
  var modules = getAllModules(raw);
  var usedLocally = usedLocallyOnly(exports, imports);
  return { imports: imports, exports: exports, private: usedLocally, modules: modules }
};

var log = console.log;
var getProjectDetails = ramda.pipe(
  ramda.chain(getImportsAndExports),
  ramda.map(transformer)
);
var getJSDocComments = ramda.pipe(
  ramda.map(ramda.chain(getComments)),
  ramda.chain(F.parallel(10)),
  ramda.map(
    ramda.pipe(
      ramda.map(ramda.filter(ramda.pathOr(false, ["tags", "length"]))),
      ramda.filter(ramda.propOr(false, "length"))
    )
  ),
  ramda.map(ramda.reduce(ramda.concat, []))
);
var binoculars = ramda.pipe(
  readArgv(CONFIG.yargsOpts),
  ramda.pathOr([], ["_", 0]),
  forceGlob,
  ramda.map(sourceFilesOnly),
  function (z) { return [getProjectDetails(z), getJSDocComments(z)]; },
  F.parallel(2),
  ramda.map(function (ref) {
    var project = ref[0];
    var comments = ref[1];
    return ({ project: project, comments: comments });
}),
  ramda.map(function (x) { return JSON.stringify(x, null, 2); }),
  fork(log, log)
);
binoculars(process.argv.slice(2));

module.exports = binoculars;
